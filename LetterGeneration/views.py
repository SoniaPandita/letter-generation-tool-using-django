# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.pagesizes import landscape
from reportlab.platypus import Image
from django.http import HttpResponse,FileResponse
from . import forms
from docx import Document
from docx.shared import Inches
import io
import errno
import os
from cStringIO import StringIO
import os.path
def letter_create(request):
	if request.method=='POST':
		form=forms.LetterGenerator(request.POST,request.FILES)
		if form.is_valid():
			instance=form.save(commit=False)
			instance.save()
			first_name = form.cleaned_data.get('firstname')
			last_name = form.cleaned_data.get('lastname')
			email_id = form.cleaned_data.get('email')
			contact_number = form.cleaned_data.get('contact')
			user_name = first_name + ' ' + last_name
			pdf_file_name = last_name+ ' ' + first_name + '.pdf'
			doc_file_name = last_name+ ' ' + first_name + '.doc'
			if request.POST.get("pdf"):		
				c = canvas.Canvas(pdf_file_name, pagesize=landscape(letter))
				# heading
				c.setFont('Helvetica', 45, leading=None)
				c.drawString(160, 500, "Jivass Technologies")
				# User name
				c.setFont('Helvetica-Bold', 20, leading=None)
				c.drawString(80, 400, user_name + " is working in Jivass Technologies.")
				#Email-ID:
				c.setFont('Helvetica', 18, leading=None)
				c.drawString(80, 360, "Email-ID:")
				#Email ID
				c.setFont('Helvetica-Bold', 20, leading=None)
				c.drawString(165, 360, email_id)
				#Contact number:
				c.setFont('Helvetica', 18, leading=None)
				c.drawString(80, 320, "Contact number:")
				#Contact number
				c.setFont('Helvetica-Bold', 20, leading=None)
				c.drawString(220, 320, contact_number)
				# logo
				logo = 'assets/Jivass_technologies.png'
				c.drawImage(logo, 50, 500, width=None, height=None)
				c.showPage()
				c.save()
				#return redirect('http://127.0.0.1:8000/letter/generate')
				return render(request,'LetterGeneration/letter_create_success.html',{})
			elif request.POST.get("doc"):
				document=Document()
				document.add_heading("Jivass Technologies")
				document.add_paragraph(user_name + " is working in Jivass Technologies")
				document.add_paragraph("Email ID:" + email_id)
				document.add_paragraph("Contact Number:" + contact_number)
				document.save(doc_file_name)
				return render(request,'LetterGeneration/letter_create_success.html',{})
			elif request.POST.get("previewpdf"):
					if os.path.isfile(pdf_file_name):
						with open(pdf_file_name, 'r') as pdf:
							response=HttpResponse(pdf.read(), content_type='application/pdf')
							response['Content-Disposition'] = 'inline;filename=pdf_file_name'
							return response	
					else:
						c = canvas.Canvas(pdf_file_name, pagesize=landscape(letter))
						c.setFont('Helvetica', 45, leading=None)
						c.drawString(160, 500, "Jivass Technologies")
						c.setFont('Helvetica-Bold', 20, leading=None)
						c.drawString(80, 400, user_name + " is working in Jivass Technologies.")
						c.setFont('Helvetica', 18, leading=None)
						c.drawString(80, 360, "Email-ID:")
						c.setFont('Helvetica-Bold', 20, leading=None)
						c.drawString(165, 360, email_id)
						c.setFont('Helvetica', 18, leading=None)
						c.drawString(80, 320, "Contact number:")
						c.setFont('Helvetica-Bold', 20, leading=None)
						c.drawString(220, 320, contact_number)
						logo = 'assets/Jivass_technologies.png'
						c.drawImage(logo, 50, 500, width=None, height=None)
						c.showPage()
						c.save()
						with open(pdf_file_name, 'r') as pdf:
							response=HttpResponse(pdf.read(), content_type='application/pdf')
							response['Content-Disposition'] = 'inline;filename=pdf_file_name'
							return response	
					
			elif request.POST.get("previewdocx"):
					try:
						with open(doc_file_name, 'rb') as docx:
							response = HttpResponse(docx.read(),content_type='application/pdf')
							response['Content-Disposition']='inline; filename=doc_file_name'
							return response
					except:
						return HttpResponse("Create one doc file")
						
	else:
		form=forms.LetterGenerator()
	return render(request,'LetterGeneration/letter_create.html',{'form':form})
