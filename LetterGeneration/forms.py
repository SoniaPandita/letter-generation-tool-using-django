from django import forms
from . import models 
class LetterGenerator(forms.ModelForm):
	class Meta:
		model=models.Data
		fields=['firstname','lastname','email','contact']
