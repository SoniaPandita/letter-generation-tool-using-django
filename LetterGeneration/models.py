# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Data(models.Model):
	firstname=models.CharField(max_length=30, default=" ")
	lastname=models.CharField(max_length=30, default=" ")
	email=models.EmailField(max_length=100, default=" ")
	contact=models.CharField(max_length=30, default=" ")

	def __str__(self):
		return self.firstname

# Create your models here.
